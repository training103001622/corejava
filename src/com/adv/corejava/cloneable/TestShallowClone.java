package com.adv.corejava.cloneable;

public class TestShallowClone {

	public static void main(String[] args) throws CloneNotSupportedException {
		ShallowClone cloneEx1 = new ShallowClone("Tiger", 100);
		System.out.println(cloneEx1);
		
		ShallowClone cloneEx3 = cloneEx1;
		System.out.println(cloneEx3);
		
		Object cloneObj = cloneEx1.clone();

		//down casting
		ShallowClone cloneEx2 = (ShallowClone)cloneObj;
		System.out.println(cloneEx2);
		
		//upcasting
		Object obj = cloneEx2;
	}
}

class ShallowClone implements Cloneable {
	
	private String name;
	
	private int age;
	
	public ShallowClone(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
//	@Override
//	public String toString() {
//		return "Name:"+getName()+", Age:"+getAge();
//	}
}