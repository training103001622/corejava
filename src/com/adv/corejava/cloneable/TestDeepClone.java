package com.adv.corejava.cloneable;

public class TestDeepClone {

	public static void main(String[] args) throws CloneNotSupportedException {
		Address address = new Address("Kgi", 678765);
		
		Student studentOriginal = new Student("Tiger", 100, address);
		System.out.println("Student Original Object ->"+studentOriginal);
		
		Object studentObjClone = studentOriginal.clone();

		//down casting
		Student studentClone1 = (Student)studentObjClone;
		System.out.println("Student Clone Object ->"+studentClone1);
		
		Address address1 = studentOriginal.getAddress();
		System.out.println("Address Original Object ->"+address1);
		Address address2 = studentClone1.getAddress();
		System.out.println("Address Clone Object ->"+address2);
	}
}

class Address implements Cloneable {
	
	private String city;
	
	private int pincode;

	public Address(String city, int pincode) {
		this.city = city;
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
		//return this;
	}
}

class Student implements Cloneable {
	
	private String name;
	
	private int age;
	
	private Address address;
	
	public Student(String name, int age, Address address) {
		this.name = name;
		this.age = age;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Student studentObj =  (Student) super.clone();
		Address address = studentObj.getAddress();
		Address addressClone = (Address) address.clone();
		studentObj.setAddress(addressClone);
		return studentObj;
	}
	
//	@Override
//	public String toString() {
//		return "Name:"+getName()+", Age:"+getAge();
//	}
}