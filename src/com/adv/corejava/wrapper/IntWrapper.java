package com.adv.corejava.wrapper;

public class IntWrapper {

	private String name;
	
	private static int aValue;
	
	public static IntWrapper valueOf(int a) {
		aValue = a;
		return new IntWrapper();
	}
	
	@Override
	public String toString() {
		return aValue+"";
	}
}
