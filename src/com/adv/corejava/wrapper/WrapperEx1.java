package com.adv.corejava.wrapper;

public class WrapperEx1 {

	public static void main(String[] args) {
		int a = 10;
		//auto-boxing
		Integer aObj = 10;
		
		Integer aObj1 = Integer.valueOf(200);
		System.out.println(aObj1.intValue());

		int c = aObj1.intValue();
		
		//auto-unboxing
		int b = aObj1;
		
		boolean chk1 = true;
		Boolean boolObj = chk1;
		boolean chk2 = boolObj;
		
	}
}
