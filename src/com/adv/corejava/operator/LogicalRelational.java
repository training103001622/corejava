package com.adv.corejava.operator;

public class LogicalRelational {

	public static void main(String[] args) {
		//Logical
		int a = 10;
		int b = 20;
		int c = 30;
		int d = 25;
		
		if ((a < b) && (c < d)) {
			System.out.println("Second Number is bigger in both scenarios");
		} else if ((a > b) && (c > d)) {
			System.out.println("First Number is bigger in both scenarios");
		} else if ((a < b) && (c > d)) {
			System.out.println("First Number is smaller in first scenario and Second Number is smaller in second scenario");
		} else if ((a > b) && (c < d)) {
			System.out.println("First Number is bigger in first scenario and Second Number is bigger in second scenario");
		}
	}
}
