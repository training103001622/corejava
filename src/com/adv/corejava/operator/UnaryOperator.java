package com.adv.corejava.operator;

public class UnaryOperator {

	public static void main(String[] args) {
		int i = 0;
		//unary
		System.out.println(i++);
		System.out.println("The value of i="+i);
		System.out.println(--i);
		
		i++;
		i= i+1;
		
		i+=5;
		i=i+5;
		
		i-=5;
		i=i-5;
		
		UnaryOperator unaryOper = new UnaryOperator();
	}
}
