package com.adv.corejava.operator;

public class ShiftOperator {

	public static void main(String[] args) {
		//Left shift operator
		System.out.println("Left shift operator");
		int leftShift1 = 10<<2;
		//10*2^2=40
		System.out.println(leftShift1);
		int leftShift2 = 10<<4;
		//10*2^4=160
		System.out.println(leftShift2);
		System.out.println("--------------------");
		
		//right shift operator
		System.out.println("Right shift operator");
		int rightShift1 = 16>>2;
		//16/2^2=40
		System.out.println(rightShift1);
		int rightShift2 = 32>>4;
		//32/2^4=160
		System.out.println(rightShift2);
		System.out.println("--------------------");
		
	}
}
