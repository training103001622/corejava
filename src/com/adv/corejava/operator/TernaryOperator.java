package com.adv.corejava.operator;

public class TernaryOperator {

	public static void main(String[] args) {
		int a = 10;
		
		String b = "";
		if (a == 10) {
			b = "Equal";
		} else {
			b = "Not-Equal";
		}
		System.out.println(b);
		
		String c = (a == 10) ? "Equal" : "Not-Equal";
		System.out.println(c);
	}
}
