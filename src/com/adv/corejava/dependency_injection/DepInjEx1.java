package com.adv.corejava.dependency_injection;

public class DepInjEx1 {

	public static void main(String[] args) {
		A a1 = new A(new B());
	}
}

class A {
	
	private B b;
	
	public A(B b) {
		this.b = b;
	}
	
	public void setB(B b) {
		this.b = b;
	}
	
	public B getB() {
		return b;
	}
}

class B {
	
}