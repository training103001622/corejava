package com.adv.corejava.immutable;

public class TestImmutableEx1 {

	public static void main(String[] args) {
		ImmutableEx1 immutableEx1 = new ImmutableEx1("Tiger");
		System.out.println(immutableEx1.getName());
	}
}

final class ImmutableEx1 {
	
	private final String name;
	
	public ImmutableEx1(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}