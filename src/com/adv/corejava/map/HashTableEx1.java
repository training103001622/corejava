package com.adv.corejava.map;

import java.util.Hashtable;

public class HashTableEx1 {

	public static void main(String[] args) {
		Hashtable<String, String> ht = new Hashtable<>();
		ht.put("S1", "101");
		ht.put("Z02", "210");
		ht.put("N81", "16");
//		ht.put("S1", "201");
		ht.putIfAbsent("S1", "201");
		ht.put("C12", "37");
		ht.put("S07", "8");
//		ht.put(null, "1");
//		ht.put(null, "2");
//		ht.put("S09", null);
//		ht.put("S11", null);
		
		System.out.println(ht);
	}
}