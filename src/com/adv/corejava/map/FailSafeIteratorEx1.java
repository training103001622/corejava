package com.adv.corejava.map;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class FailSafeIteratorEx1 {

	public static void main(String[] args) {
		ConcurrentHashMap<String, String> chm = new ConcurrentHashMap<String, String>();
		chm.put("A", "A1");
		chm.put("B", "B1");
		chm.put("A", "A2");
		chm.put("C", "C1");
		chm.put("D", "D1");
		
		Iterator<Entry<String, String>> entryIterator = chm.entrySet().iterator();
		while (entryIterator.hasNext()) {
			Entry<String, String> entry = entryIterator.next();
			String key = entry.getKey();
			String value = entry.getValue();
			System.out.println("Key="+key+", Value="+value);
			chm.put("E", "E1");
		}
	}
}
