package com.adv.corejava.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class FailFastIteratorEx1 {

	public static void main(String[] args) {
		HashMap<String, String> hm = new HashMap<>();
		hm.put("A", "A1");
		hm.put("B", "B1");
		hm.put("A", "A2");
		hm.put("C", "C1");
		hm.put("D", "D1");
		
		Iterator<Entry<String, String>> entryIterator = hm.entrySet().iterator();
		while (entryIterator.hasNext()) {
			Entry<String, String> entry = entryIterator.next();
			String key = entry.getKey();
			String value = entry.getValue();
			System.out.println("Key="+key+", Value="+value);
			hm.put("E", "E1");
		}
	}
}
