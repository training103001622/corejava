package com.adv.corejava.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HashMapEx1 {

	public static void main(String[] args) {
		Map<String, String> hm = new HashMap<>();
		hm.put("S1", "101");
		hm.put("Z02", "210");
		hm.put("N81", "16");
		hm.put("S1", "201");
		hm.put("C12", "37");
		hm.put("S07", "8");
		hm.put(null, "1");
		hm.put(null, "2");
		hm.put("S09", null);
		hm.put("S11", null);
		
		System.out.println(hm);
		
		String val = hm.get("C12");
		System.out.println("Value ="+val);
		
		interateMap(hm);
	}
	
	public static void interateMap(Map<String, String> hm) {
		Set<Entry<String, String>> entrySet = hm.entrySet();
		Iterator<Entry<String, String>> entryIterator = entrySet.iterator();
		while (entryIterator.hasNext()) {
			Entry<String, String> entryObj = entryIterator.next();
			String key = entryObj.getKey();
			String value = entryObj.getValue();
			System.out.println("Key="+key+", value="+value);
		}
	}
}
