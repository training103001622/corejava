package com.adv.corejava.map;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapEx1 {

	public static void main(String[] args) {
		ConcurrentHashMap<String, String> chm = new ConcurrentHashMap<>();
		chm.put("A", "Tiger");
		chm.put("B", "Zebra");
		chm.put("B", "Lion");
//		chm.put(null, null);
//		chm.put(null, null);
		chm.put("C", "Fox");
//		chm.put("D", null);
		System.out.println(chm);
	}
}
