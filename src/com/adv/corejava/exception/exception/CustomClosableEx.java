package com.adv.corejava.exception.exception;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;

public class CustomClosableEx implements Closeable {

	private FileInputStream fis = null;
	
	public CustomClosableEx(FileInputStream fis) {
		this.fis = fis;
	}
	
	public void m1() {
		System.out.println("Inside m1()");
	}
	
	@Override
	public void close() throws IOException {
		System.out.println("Inside close() method...........");
		if (fis != null) {
			fis.close();
		}
	}
}
