package com.adv.corejava.exception.exception;

public class TestCustomCheckedExp {

	public void m1() throws CustomCheckedException {
		System.out.println("Inside TestCustomCheckedExp - m1()");
	}
	
	public void m2() throws CustomCheckedException {
		throw new CustomCheckedException("Exp Occured in m2", "TCCE01");
	}
	
	public static void main(String[] args) {
		TestCustomCheckedExp testCustomCheckedExp = new TestCustomCheckedExp();
		try {
			testCustomCheckedExp.m1();
		} catch (CustomCheckedException e) {
			e.printStackTrace();
		}
		try {
			testCustomCheckedExp.m2();
		} catch (CustomCheckedException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
