package com.adv.corejava.exception.exception;

import java.io.FileInputStream;

public class CustomAutoClosableEx implements AutoCloseable {

	private FileInputStream fis = null;
	
	public CustomAutoClosableEx(FileInputStream fis) {
		this.fis = fis;
	}
	
	public void m1() {
		System.out.println("Inside m1()");
	}
	
	@Override
	public void close() throws Exception {
		System.out.println("Inside close() method...........");
		if (fis != null) {
			fis.close();
		}
	}
}
