package com.adv.corejava.exception.exception;

public class UnCheckedException {

	public int divOperation(int a, int b) {
		int result = a/b;
		return result;
	}
	
	public int divOperationCorrected(int a, int b) {
		if (b != 0) {
			int result = a/b;
			return result;
		} else {
			throw new RuntimeException("Invalid input");
		}
	}
}
