package com.adv.corejava.exception.exception;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CheckedExceptionEx1 {

	// Let the system handle the exception
	public void readTheFile() throws Exception {
		FileInputStream fis = new FileInputStream("");
		fis.close();
	}

	// Let the user handle the exception
	public void readFileExp() {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("C:\\\\Users\\\\ADVENTO TECH\\\\OneDrive\\\\Desktop\\\\Sample.txt");
			// fis.close();
		} catch (Exception fileNotFoundEx) {
			fileNotFoundEx.printStackTrace();
			// fis.close();
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// Let the user handle the exception
	public void readFileMultiCatch() {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("");
			fis.close();
		} catch (FileNotFoundException fileNotFoundEx) {
			fileNotFoundEx.printStackTrace();
			// fis.close();
		} catch (IOException ioEx) {
			ioEx.printStackTrace();
			// fis.close();
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// Let the user handle the exception
	public void readFileMultiExceptionCatch() {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("");
			Class.forName("");
			fis.close();
		} catch (ClassNotFoundException | IOException fileIoEx) {
			fileIoEx.printStackTrace();
			// fis.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			// fis.close();
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// Let the system handle the exception
	public void readFileMultiThrows() throws ClassNotFoundException, IOException {
		FileInputStream fis = null;
		fis = new FileInputStream("");
		Class.forName("");
		fis.close();
	}
	
	// Let the system handle the exception
	public void readFileTryWithResource() {
		File file = new File("C:\\Users\\ADVENTO TECH\\OneDrive\\Desktop\\Sample.txt");
		try (BufferedReader br = new BufferedReader(new FileReader(file));) {
			String st = null;
			while ((st = br.readLine()) != null)
				System.out.println(st);
		} catch (IOException fiEx) {
			fiEx.printStackTrace();
		}
	}
	
	// Let the system handle the exception
	public void chkCustAutoCloseTryWithRes() throws Exception {
		try (CustomAutoClosableEx customAutoClosableEx = new CustomAutoClosableEx(new FileInputStream("C:\\Users\\ADVENTO TECH\\OneDrive\\Desktop\\Sample.txt"));) {
			customAutoClosableEx.m1();
		}
	}
	
	public void chkCustCloseTryWithRes() throws Exception {
		try (CustomClosableEx customClosableEx = new CustomClosableEx(new FileInputStream("C:\\Users\\ADVENTO TECH\\OneDrive\\Desktop\\Sample.txt"));) {
			customClosableEx.m1();
		}
	}
}
