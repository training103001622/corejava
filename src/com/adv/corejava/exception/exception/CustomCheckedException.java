package com.adv.corejava.exception.exception;

public class CustomCheckedException extends Exception {

	private static final long serialVersionUID = 1L;

	private String errMsg;
	
	private String errCode;

	public CustomCheckedException(String errMsg, String errCode) {
		super(errMsg);
		this.errMsg = errMsg;
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
}
