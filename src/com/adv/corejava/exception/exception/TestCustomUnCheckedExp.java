package com.adv.corejava.exception.exception;

public class TestCustomUnCheckedExp {

	public void m1() throws CustomUnCheckedException {
		System.out.println("Inside TestCustomUnCheckedExp - m1()");
	}
	
	public void m2() throws CustomUnCheckedException {
		throw new CustomUnCheckedException("Exp Occured in m2", "TCCE01");
	}
	
	public static void main(String[] args) {
		TestCustomUnCheckedExp testCustomUnCheckedExp = new TestCustomUnCheckedExp();
		testCustomUnCheckedExp.m1();
		testCustomUnCheckedExp.m2();
	}
}
