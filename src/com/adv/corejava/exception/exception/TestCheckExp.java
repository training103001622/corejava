package com.adv.corejava.exception.exception;

public class TestCheckExp {

	public static void main(String[] args) throws Exception {
		CheckedExceptionEx1 checkedExceptionEx1 = new CheckedExceptionEx1();
		checkedExceptionEx1.readFileExp();
		checkedExceptionEx1.readFileTryWithResource();
		System.out.println("----------AutoCloseable----------");
		checkedExceptionEx1.chkCustAutoCloseTryWithRes();
		System.out.println("----------Closeable----------");
		checkedExceptionEx1.chkCustCloseTryWithRes();
	}
}
