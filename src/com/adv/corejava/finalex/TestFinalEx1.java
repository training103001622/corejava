package com.adv.corejava.finalex;

public class TestFinalEx1 {

	public static void main(String[] args) {
//		FinalEx1 finalEx1 = new SubCl();
//		finalEx1.m1();
		
	}
}

//class level
final class FinalEx1 {
	
	public void m1() {
		System.out.println("Inside FinalEx1 - m1()");
	}
}

class SubClEx1 /* extends FinalEx1 */{
	
//	@Override
//	public void m1() {
//		System.out.println("Inside SubClEx1 - m1()");
//	}
}

//method level
class FinalEx2 {
	
	public final void m1() {
		System.out.println("Inside FinalEx2 - m1()");
	}
	
	public void m2() {
		System.out.println("Inside FinalEx2 - m2()");
	}
}

class SubClEx2 extends FinalEx2 {
	
//	@Override
//	public void m1() {
//		System.out.println("Inside SubClEx2 - m1()");
//	}
	
	@Override
	public void m2() {
		System.out.println("Inside SubClEx2 - m2()");
	}
}

//Variable level
class FinalEx3 {
	
//	public final String name = "";
	
	public final String name;
	
	public FinalEx3(String name) {
		this.name = name;
	}
	
//	public void setName(String name) {
//		this.name = name;
//	}
}
