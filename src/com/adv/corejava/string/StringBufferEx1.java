package com.adv.corejava.string;

public class StringBufferEx1 {

	public static void main(String[] args) {
		StringBuffer stBuff = new StringBuffer("Lion");
		StringBuffer stBuff1 = stBuff;
		
		stBuff.append("Tiger");
		System.out.println(stBuff);
		
		if (stBuff == stBuff1) {
			System.out.println("Both Objects are equal");
		} else {
			System.out.println("Both Objects are not equal");
		}
		
		//convert the stringbuffer to string
		String s1 = stBuff.toString();
		
		//String reverse
		String strReverse = stBuff.reverse().toString();
		System.out.println("strReverse="+strReverse);
	}
}
