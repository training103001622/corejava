package com.adv.corejava.string;

public class StringEx1 {

	public static void reverseString(String input) {
		String output = "";
		for (int i=input.length()-1; i>=0; i--) {
			//output += input.charAt(i);
			output = output.concat(input.charAt(i)+"");
			System.out.println(output);
		}
		System.out.println("String reverse value="+output);
	}
	
	public static void main(String[] args) {
		reverseString("Tiger");
		//String object - using new keyword
		String s1 = new String("Tiger");
		String s5 = s1;
		System.out.println("Before Change.......s1="+s1+", s5="+s5);
		if (s1 == s5) {
			System.out.println("Equal....");
		} else {
			System.out.println("Not Equal....");
		}
		s1 = s1.concat("I");
		
		System.out.println("After Change.......s1="+s1+", s5="+s5);
		if (s1 == s5) {
			System.out.println("Equal....");
		} else {
			System.out.println("Not Equal....");
		}
		
		String s2 = new String("Tiger");
		
		String sample = "Hi Hello World , Welcome";
		
		String subStr = sample.substring(2, 15);
		System.out.println("subStr="+subStr.trim());
		
		char[] strArr = s1.toCharArray();
		System.out.println("String in Array="+strArr);
		
		//checks the memory address
		System.out.println("compares the memory address");
		if (s1 == s2) {
			System.out.println("s1 & s2 are equal");
		} else {
			System.out.println("s1 & s2 are not equal");
		}
		System.out.println("-------------------------------");

		//checks the content
		System.out.println("compares the content");
		if (s1.equals(s2)) {
			System.out.println("s1 & s2 are equal");
		} else {
			System.out.println("s1 & s2 are not equal");
		}
		System.out.println("-------------------------------");

		//checks the memory address with intern
		System.out.println("compares the memory address with intern");
		if (s1.intern() == s2.intern()) {
			System.out.println("s1 & s2 are equal");
		} else {
			System.out.println("s1 & s2 are not equal");
		}

		//String object - using literals
		String s3 = "Zebra";
		String s4 = "Zebra";

		System.out.println("-------------------------------");

		//checks the memory address
		System.out.println("compares the memory address");
		if (s3 == s4) {
			System.out.println("s3 & s4 are equal");
		} else {
			System.out.println("s3 & s4 are not equal");
		}
		
		//checks the content
		System.out.println("-------------------------------");
		System.out.println("compares the content");
		if (s3.equals(s4)) {
			System.out.println("s3 & s4 are equal");
		} else {
			System.out.println("s3 & s4 are not equal");
		}
	}
}
