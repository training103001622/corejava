package com.adv.corejava.string;

public class StringSplitEx1 {

	public static void main(String[] args) {
		String str = "Hi,Hello World,Welcome to Split Learning";
		String strArr[] = str.split(",");
		for (int i = 0; i < strArr.length; i++) {
			System.out.println(strArr[i]);
		}
	}
}
