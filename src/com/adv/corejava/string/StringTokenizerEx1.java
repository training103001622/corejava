package com.adv.corejava.string;

import java.util.StringTokenizer;

public class StringTokenizerEx1 {

	public static void main(String[] args) {
		String st = "Hi,Hello World,welcome to training";
		StringTokenizer stTokens = new StringTokenizer(st, ",");
		while (stTokens.hasMoreTokens()) {
			String token = stTokens.nextToken();
			System.out.println(token);
		}
	}
}
