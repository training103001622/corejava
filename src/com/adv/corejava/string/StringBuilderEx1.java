package com.adv.corejava.string;

public class StringBuilderEx1 {

	public static void main(String[] args) {
		StringBuilder stBuild = new StringBuilder("Lion");
		StringBuilder stBuild1 = stBuild;
		
		stBuild.append("Tiger");
		System.out.println(stBuild);
		
		if (stBuild == stBuild1) {
			System.out.println("Both Objects are equal");
		} else {
			System.out.println("Both Objects are not equal");
		}
		
		//convert the StringBuilder to string
		String s1 = stBuild.toString();
		
		//String reverse
		String strReverse = stBuild.reverse().toString();
		System.out.println("strReverse="+strReverse);
	}
}
