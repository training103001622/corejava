package com.adv.corejava.conditions;

public class TernaryEx1 {

	public static void main(String[] args) {
		ifElse(17);
		System.out.println("-------------");
		ternary(19);
	}
	
	public static void ifElse(int age) {
		if (age >= 18) {
			System.out.println("Major");
		} else {
			System.out.println("Minor");
		}
	}
	
	public static void ternary(int age) {
		String result = (age >= 18) ? "Major" : "Minor";
		System.out.println(result);
	}
}
