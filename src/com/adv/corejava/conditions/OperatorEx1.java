package com.adv.corejava.conditions;

import java.util.Scanner;

public class OperatorEx1 {
	
	public static void main(String ar[]) {
		
		String name = "\nHi,\nHow r \bu.\n\"Welcome to this world.\"\n";
		System.out.println("name="+name);
		
		Scanner input = new Scanner(System.in);
		//String value = input.nextLine();//String input
		int value = input.nextInt();//integer input
		
		if (value == 18 || value > 18) {
			System.out.println("Eligible for vote......");
		} else {
			System.out.println("Not Eligible for vote......");
		}
	}
}