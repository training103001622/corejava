package com.adv.corejava.multithreading;

public class TestThreadEx1 {

	public static void main(String[] args) {
		ThreadEx1 threadEx1 = new ThreadEx1();
		ThreadEx2 threadEx2 = new ThreadEx2();

		Thread t1 = new Thread(threadEx1);
		Thread t2 = new Thread(threadEx1);
		
//		threadEx1.start();
		
		Thread t3 = new Thread(threadEx2);
		Thread t4 = new Thread(threadEx2);
		
		t1.start();
		t2.start();
		
		t3.start();
		t4.start();
	}
}
	
class ThreadEx1 extends Thread {
	
	@Override
	public void run() {
		m1();
		m2();
	}
	
	public void m1() {
		System.out.println("Inside m1-Start. ThreadName="+Thread.currentThread().getName());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Inside m1-End. ThreadName="+Thread.currentThread().getName());
	}
	
	public void m2() {
		System.out.println("Inside m2");
	}
}

class ThreadEx2 implements Runnable {
	
	@Override
	public void run() {
		m3();
		m4();
	}
	
	public synchronized void m3() {
		System.out.println("Inside m3-Start. ThreadName="+Thread.currentThread().getName());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Inside m3-End. ThreadName="+Thread.currentThread().getName());
	}
	
	public synchronized void m4() {
		System.out.println("Inside m4");
	}
}