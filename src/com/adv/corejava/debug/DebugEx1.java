package com.adv.corejava.debug;

public class DebugEx1 {

	public static void printArray(int arr[]) {
		for (int i=0; i<arr.length; i+=2) {
			System.out.println("Array value="+arr[i]);
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Started");
		int a[] = {5,3,7,10,2,11};
		printArray(a);
		System.out.println("Done");
	}
}
