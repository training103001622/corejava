package com.adv.corejava.object;

public class TestObjectEx1 extends Object {

	public static void main(String[] args) {
		String s1 = "Tiger";
		String s2 = "Zebra";
		
		boolean chk = s1.equals(s2);
		
		ObjectEx1 objectEx1 = new ObjectEx1(s1, 10);
		System.out.println("objectEx1 :: "+objectEx1.toString());
		System.out.println("objectEx1 :: "+objectEx1);
		
		System.out.println("HashCode Value="+objectEx1.hashCode());
	}
}

class ObjectEx1 {
	
	private String name;
	
	private int age;
	
	public ObjectEx1(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	@Override
	public int hashCode() {
		int hashValue = name.hashCode() + age;
		return hashValue;
	}
	
	@Override
	public String toString() {
		String output = "name= "+this.name + ", age="+this.age;
		return output;
	}
}