package com.adv.corejava.oops.access_specifier_test;

import com.adv.corejava.oops.access_specifier.AccessSpecifierEx1;

public class AccSpecProtectedEx2 {

	public void m2() {
		AccessSpecifierEx1 accessSpecifierEx1 = new AccessSpecifierEx1();
		//accessSpecifierEx1.age;
		//int ageVal = age;
		String fullName = accessSpecifierEx1.fullName;
		System.out.println("FullName="+fullName);
	}
	
	public static void main(String[] args) {
		AccSpecProtectedEx2 accSpecProtectedEx2 = new AccSpecProtectedEx2();
		accSpecProtectedEx2.m2();
	}
}
