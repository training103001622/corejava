package com.adv.corejava.oops.access_specifier;

public class AccessSpecifierEx1 {

	//Accessibility with the project
	public String fullName = "TigerRaja";
	
	//Accessibility within the class
	private String name = "Lion";
	
	//Accessibility within the package (package private)
	String email= "t1@g.com";
	
	//Accessibility within package and sub class of different package
	protected int age = 10;
	
	public void m1() {
		System.out.println("name="+name);
	}
}
