package com.adv.corejava.oops.access_specifier;

public class TestAccessSpecifierEx1 {

	public static void main(String[] args) {
		AccessSpecifierEx1 accessSpecifierEx1 = new AccessSpecifierEx1();
		accessSpecifierEx1.m1();
		
		//System.out.println(accessSpecifierEx1.name);
		
		//String name = accessSpecifierEx1.name;
		//System.out.println("name="+name);
		
		String email = accessSpecifierEx1.email;
		System.out.println("email="+email);
	}
}
