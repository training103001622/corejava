package com.adv.corejava.oops.abstraction;

public class MultipleInheritanceEx1 implements InterfaceEx1, InterfaceEx2 {

	@Override
	public void m2() {
		//System.out.println(FIRST_NAME);
		String fName = InterfaceEx1.FIRST_NAME;
		System.out.println("FirstName="+fName);
		System.out.println("Inside MultipleInheritanceEx1 - m2()");
	}

	@Override
	public void m1() {
		System.out.println("Inside MultipleInheritanceEx1 - m1()");
	}
}
