package com.adv.corejava.oops.abstraction;

public interface InterfaceEx2 {

	public static final String FIRST_NAME = "Lion";
	
	public abstract void m2();
	
	public default void m1() {
		System.out.println("Inside InterfaceEx1::m1 - Default method");
	}
}
