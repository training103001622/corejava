package com.adv.corejava.oops.abstraction;

public class TestAbstractionEx1 {
	
	public static void main(String[] args) {
		AbsClsEx1 absClsEx1 = new AbsClsImpl();
		absClsEx1.m2();
	}
}

abstract class AbsClsEx1 {
	
	private String name;
	
	public AbsClsEx1() {
	}
	
//	public AbsClsEx1(String name) {
//		this.name = name;
//	}

	public void m1() {
		System.out.println("Inside AbstractionEx1 - m1()");
	}
	
	public abstract void m2();
}

class AbsClsImpl extends AbsClsEx1 {

	public AbsClsImpl() {
		
	}
	
	@Override
	public void m2() {
		System.out.println("Inside AbsClsEx1 - m2()");
	}
}