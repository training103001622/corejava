package com.adv.corejava.oops.abstraction;

public interface InterfaceEx1 {

	public static final String FIRST_NAME = "Tiger";
	
	public abstract void m2();
	
	public static void m3() {
		System.out.println("Inside InterfaceEx1::m3 - static method");
	}
	
	public default void m1() {
		System.out.println("Inside InterfaceEx1::m1 - Default method");
	}
}
