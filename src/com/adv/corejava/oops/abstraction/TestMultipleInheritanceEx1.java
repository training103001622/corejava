package com.adv.corejava.oops.abstraction;

public class TestMultipleInheritanceEx1 {

	public static void main(String[] args) {
		InterfaceEx1 interfaceEx1 = new MultipleInheritanceEx1();
		interfaceEx1.m1();
		interfaceEx1.m2();
		
		InterfaceEx1.m3();
	}
}
