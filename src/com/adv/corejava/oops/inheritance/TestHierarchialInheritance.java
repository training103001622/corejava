package com.adv.corejava.oops.inheritance;

public class TestHierarchialInheritance {
	
	public static void main(String[] args) {
		Child1Hier child1Hier = new Child1Hier();
		child1Hier.m1();
		child1Hier.m2();
		System.out.println(child1Hier.name);
		
		Child2Hier child2Hier = new Child2Hier();
		child2Hier.m1();
		child2Hier.m3();
		System.out.println(child2Hier.name);
		
		Child3Hier child3Hier = new Child3Hier();
		child3Hier.m1();
		child3Hier.m4();
		System.out.println(child3Hier.name);
	}
}

class ParentHier {
	
	public String name = "Lion";
	
	public void m1() {
		System.out.println("Inside ParentHier - m1()");
	}
}

class Child1Hier extends ParentHier {
	
	public void m2() {
		System.out.println("Inside Child1Hier - m2()");
	}
}

class Child2Hier extends ParentHier {
	
	public void m3() {
		System.out.println("Inside Child2Hier - m3()");
	}
}

class Child3Hier extends ParentHier {
	
	public void m4() {
		System.out.println("Inside Child3Hier - m4()");
	}
}