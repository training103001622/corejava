package com.adv.corejava.oops.inheritance;

public class TestSingleInheritance {

	public static void main(String[] args) {
//		Parent p1 = new Parent();
//		System.out.println("Name="+p1.name+", age="+p1.age);
		Child1 c1 = new Child1();
		System.out.println("Name="+c1.name+", age="+c1.age);
		c1.m1();
		c1.m2();
	}
}

class Parent {

	public String name;
	
	public int age;
	
	public Parent() {
		System.out.println("Inside Parent constructor......");
		name = "Tiger";
		age = 10;
	}
	
	public void m1() {
		System.out.println("Inside the Parent - m1()........");
	}
}

class Child1 extends Parent {
	
	public Child1() {
		//super();
		System.out.println("Inside Child constructor......");
	}
	
	public void m2() {
		System.out.println("Inside the child - m2()..........");
	}
}
