package com.adv.corejava.oops.inheritance;

public class TestMultipleInheritance {
	
	public static void main(String[] args) {
		Child child = new Child();
		child.m2();
	}
}

class Parent1 {
	
	public String name = "Lion";
	
	public void m1() {
		System.out.println("Inside Parent1 - m1()");
	}
}

class Parent2 {
	
	public String name = "Tiger";
	
	public void m1() {
		System.out.println("Inside Parent2 - m1()");
	}
}

class Child extends Parent1/*, Parent2*/ {
	
	public void m2() {
		m1();
		System.out.println(name);
	}
}