package com.adv.corejava.oops.access_modifiers;

public final class FinalClsEx1 {
	
}

// final class can't be extended (sub-class)
/*class Child extends FinalClsEx1 {
	
}*/