package com.adv.corejava.oops.access_modifiers;

public class TestFinalVariableEx2 {

	public static void main(String[] args) {
		FinalVariableEx1 finalVariableEx1 = new FinalVariableEx1("Zebra");
		String name = finalVariableEx1.getName();
		System.out.println("Name="+name);
		finalVariableEx1.setName("Fox");
		name = finalVariableEx1.getName();
		System.out.println("Name="+name);
	}
}
