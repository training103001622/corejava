package com.adv.corejava.oops.access_modifiers;

public class FinalMethodEx1 {

	public final void m1() {
		System.out.println("Inside FinalMethodEx1 - m1()");
	}

	public void m2() {
		System.out.println("Inside FinalMethodEx1 - m2()");
	}
}

class Child1 extends FinalMethodEx1 {

// Final method can't be overridden
/*	@Override
	public void m1() {
		System.out.println("Inside Child1 - m1()");
	}*/
}