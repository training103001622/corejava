package com.adv.corejava.oops.access_modifiers;

public class StaticEx1 {

	public static String name;
	
	public static int age;
	
	//static block
	static {
		System.out.println("Inside static block........");
		name = "Jimmi";
		age = 8;
	}
	
	{
		System.out.println("Inside instance block........");
	}
	
	public StaticEx1(String name, int age) {
		System.out.println("Inside constructor.......");
		this.name = name;
		this.age = age;
	}
	
	public void m1() {
		System.out.println("name="+name);
		System.out.println("age="+age);
	}
	
	public static void m2() {
		System.out.println("Inside static method m2-----");
	}
}
