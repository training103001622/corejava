package com.adv.corejava.oops.access_modifiers;

public class FinalVariableEx1 {

	//direct initialization
	private final String name = "Tiger";
	
	//block initialization
	{
		//this.name = "Lion";
	}
	
	//constructor initialization
	public FinalVariableEx1(String name) {
		//this.name = name;
	}
	
	//getter - to get the value from the variable
	public String getName() {
		return name;
	}
	
	//setter initialization
	public void setName(String name) {
		//this.name = name;
	}
}
