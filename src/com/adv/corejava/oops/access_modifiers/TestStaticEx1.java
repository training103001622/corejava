package com.adv.corejava.oops.access_modifiers;

public class TestStaticEx1 {

	public static void main(String[] args) {
		StaticEx1 staticEx1 = new StaticEx1("Lion", 10);
		StaticEx1 staticEx2 = new StaticEx1("Fox", 4);
		StaticEx1 staticEx3 = new StaticEx1("Zebra", 3);
		StaticEx1 staticEx4 = new StaticEx1("Buffallo", 11);
		StaticEx1 staticEx5 = new StaticEx1("Cow", 15);
		StaticEx1 staticEx6 = new StaticEx1("Cat", 5);
		
		staticEx1.m1();
		String nameVal = staticEx1.name;
		int ageVal = staticEx1.age;
		System.out.println("Length = "+nameVal.length());
		System.out.println("age = "+ageVal);
		
		System.out.println("---------------------------");
		
		System.out.println("Object 1 :: Name="+staticEx1.name+", age="+staticEx1.age);
		System.out.println("Object 2 :: Name="+staticEx2.name+", age="+staticEx2.age);
		System.out.println("Object 3 :: Name="+staticEx3.name+", age="+staticEx3.age);
		System.out.println("Object 4 :: Name="+staticEx4.name+", age="+staticEx4.age);
		System.out.println("Object 5 :: Name="+staticEx5.name+", age="+staticEx5.age);
	}
}
