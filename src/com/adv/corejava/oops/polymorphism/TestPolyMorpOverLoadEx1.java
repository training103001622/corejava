package com.adv.corejava.oops.polymorphism;

public class TestPolyMorpOverLoadEx1 {

	public static void main(String[] args) {
		//Child Object
		Child c1 = new Child();
		int res1 = c1.add();
		System.out.println(res1);
		System.out.println("-------------");
		//varargs
		float res2 = c1.add(1.0f, 0.5f, 4.3f);
		System.out.println(res2);
		float res3 = c1.add(1.0f, 0.5f, 4.3f, 6.2f, 7.8f);
		System.out.println();
		float res4 = c1.add(1.0f);
		System.out.println(res4);
		float res5 = c1.add(1.0f, 1.1f);
		System.out.println(res5);
		System.out.println("-------------");
		
		int a[] = {3,2,4,7,9};
		int res6 = c1.add(a);
		System.out.println(res6);
	}
}

class Parent {
	
	public int add(int arr[]) {
		int res = 0;
		for (int i = 0; i<arr.length; i++) {
			res += arr[i];
		}
		return res;
	}
}

class Child extends Parent {
	
	public int add() {
		return 5 + 10;
	}
	
	public int add(int a, int b) {
		return a + b;
	}
	
	//varargs
	public float add(float ...ar) {
		float res = 0.0f;
		for (int i = 0; i<ar.length; i++) {
			res += ar[i];
		}
		return res;
	}
	
	public int add(int a, int b, int c) {
		return a + b + c;
	}
	
	public int add(int a, int b, int c, int d) {
		return a + b + c + d;
	}
}