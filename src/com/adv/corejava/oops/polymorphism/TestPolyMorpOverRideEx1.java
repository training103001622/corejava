package com.adv.corejava.oops.polymorphism;

public class TestPolyMorpOverRideEx1 {

	public static void main(String[] args) {
		Child1 c2 = new Child1();
		String str1 = c2.print("Lion");
		System.out.println(str1);
		Parent1 p2 = new Parent1();
		String str2 = p2.print("Zebra");
		System.out.println(str2);
		
		Parent1 p1 = new Child1();
		System.out.println(p1.print("Tiger"));
		
		TestPolyMorpOverRideEx1 testPolyMorpOverRideEx1 = new TestPolyMorpOverRideEx1();
		testPolyMorpOverRideEx1.m1(new Child1());
		testPolyMorpOverRideEx1.m1(new Child2());
		testPolyMorpOverRideEx1.m1(new Child3());
	}
	
	public void m1(Parent1 p) {
		System.out.println(p.print("Tiger"));
	}
}

class Parent1 {
	
	public String print(String name) {
		return "Hi, Hello " + name;
	}
}

class Child1 extends Parent1 {
	
	public String print(String name) {
		return "Hi "+name +", Welcome to Training.....Child -1..";
	}
}

class Child2 extends Parent1 {
	
	public String print(String name) {
		return "Hi "+name +", Welcome to Training.....Child -2..";
	}
}

class Child3 extends Parent1 {
	
	public String print(String name) {
		return "Hi "+name +", Welcome to Training.....Child -3..";
	}
}