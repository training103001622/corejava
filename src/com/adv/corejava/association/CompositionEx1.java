package com.adv.corejava.association;

public class CompositionEx1 {

	public static void main(String[] args) {
		Head head = new Head();
	}
}

class Head {

	private Body body;

	public Head() {
		this.body = new Body();
	}

}

class Body {

}