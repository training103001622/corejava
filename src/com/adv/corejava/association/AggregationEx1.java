package com.adv.corejava.association;

public class AggregationEx1 {
	
	public static void main(String[] args) {
		School school = new School();
		Student student = new Student(school);
		Student student1 = new Student(school);
	}
}

class Student {
	
	private School school;
	
	public Student(School school) {
		this.school = school;
	}
}

class School {

}