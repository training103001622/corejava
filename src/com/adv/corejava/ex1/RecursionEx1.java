package com.adv.corejava.ex1;

public class RecursionEx1 {

	public static void m1(int i) {
		if (i == 0) {
			return ;
		}
		System.out.println(i);
		m1(i--);
//		i--;
//		m1(i);
	}
	
	public static void main(String[] args) {
		m1(10);
	}
}
