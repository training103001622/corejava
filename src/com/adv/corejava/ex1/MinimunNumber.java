package com.adv.corejava.ex1;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class MinimunNumber {

	public static void main(String[] args) {
		Set<Integer> mainSet = new HashSet<>();
		for (int i = 0; i < 1000; i++) {
			mainSet.add(i);
		}
		
		Set<Integer> subSet = new HashSet<>();
		subSet.add(0);
		subSet.add(1);
		//subSet.add(2);
		subSet.add(4);
		subSet.add(10);
		subSet.add(3);
		
		SortedSet<Integer> balanceSet = new TreeSet<>();
		balanceSet.addAll(mainSet);
		balanceSet.removeAll(subSet);
		System.out.println(balanceSet.first());
	}
}
