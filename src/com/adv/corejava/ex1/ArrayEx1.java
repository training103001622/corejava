package com.adv.corejava.ex1;
public class ArrayEx1 {

	public static void main(String[] args) {
		ArrayEx1 arrayEx1 = new ArrayEx1();
		int[] val = arrayEx1.evenNumbers(101, 200);
		arrayEx1.printArray(val);
	}
	
	public int[] evenNumbers(int start, int end) {
		int arrLenght = (end+1-start)/2;
		int a[] = new int[arrLenght];
		int arrCount = 0;
		if (start < end) {
			for (int i = start; i <= end; i++) {
				if (i%2 == 0) {
					a[arrCount++] = i;
				}
			}
		} else {
			System.out.println("Invalid input");
		}
		return a;
	}
	
	public void printArray(int arr[]) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+", ");
		}
		System.out.println("");
	}
	
	public int sum(int a, int b) {
		int c = a + b;
		return c;
	}
}
