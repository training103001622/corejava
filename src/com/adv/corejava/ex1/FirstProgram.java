package com.adv.corejava.ex1;

public class FirstProgram {
	
	public void m1() {
		System.out.println("Inside the FirstProgram-m1()");
	}
	
	public static void m2() {
		System.out.println("Inside the FirstProgram-m2 static method");
	}
	
	public void m3(int age) {
		if (age > 18) {
			System.out.println("Age::" + age + ", Major.......");
		} else {
			System.out.println("Age::" + age + ", Minor.......");
		}
	}
}
