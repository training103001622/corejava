package com.adv.corejava.collection;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetCustomObj {

	public static void main(String[] args) {
		SortedSet<Emp> sortSet = new TreeSet<Emp>();
		Emp e1 = new Emp("Suresh", 10);
		Emp e2 = new Emp("Anand", 15);
		Emp e3 = new Emp("Chinna", 3);
		Emp e4 = new Emp("Babu", 7);
		Emp e5 = new Emp("Arul", 41);
		
		sortSet.add(e1);
		sortSet.add(e2);
		sortSet.add(e3);
		sortSet.add(e4);
		sortSet.add(e5);

		System.out.println(sortSet);
		
		System.out.println("Iterate using for iterator......");
		Iterator<Emp> iteratorObj = sortSet.iterator();
		while (iteratorObj.hasNext()) {
			Emp emp = iteratorObj.next();
			System.out.println(emp);
		}
	}
}

class Emp implements Comparable<Emp> {
	
	private String name;
	
	private int age;

	public Emp(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "Emp [name=" + name + ", age=" + age + "]";
	}

	@Override
	public int compareTo(Emp o) {
		return this.getName().compareTo(o.getName());
	}
}
