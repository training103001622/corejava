package com.adv.corejava.collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetCustomObj {

	public static void main(String[] args) {
		Set<Teacher> set = new HashSet<>();
		set.add(new Teacher("A", 10, "a@g.com"));
		set.add(new Teacher("B", 11, "b@g.com"));
		set.add(new Teacher("A1", 13, "a@g.com"));
		set.add(new Teacher("C", 8, "c@g.com"));
		set.add(new Teacher("A2", 7, "a@g.com"));
		
		System.out.println("size="+set.size());
		System.out.println(set);
		
//		System.out.println("The value in set -> "+set);
//		
//		System.out.println("Iterate using for iterator......");
//		Iterator<String> iteratorObj = set.iterator();
//		while (iteratorObj.hasNext()) {
//			String str = iteratorObj.next();
//			System.out.println(str);
//		}
	}
}

class Teacher {
	
	private String name;
	
	private int age;
	
	private String email;

	public Teacher(String name, int age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//Email comparison
//	@Override
//	public int hashCode() {
//		return this.getEmail().hashCode();
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		Teacher teacherObj = (Teacher) obj;
//		return this.getEmail().equals(teacherObj.getEmail());
//	}

	//Name comparison
//	@Override
//	public int hashCode() {
//		return this.getName().hashCode();
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		Teacher teacherObj = (Teacher) obj;
//		return this.getName().equals(teacherObj.getName());
//	}
	
	//Age comparison
//	@Override
//	public int hashCode() {
//		return this.getAge();
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		Teacher teacherObj = (Teacher) obj;
//		return this.getAge() == teacherObj.getAge();
//	}
	
	//name, age, email comparison
	@Override
	public int hashCode() {
		return (this.getName()+this.getAge()+this.getEmail()).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Teacher teacherObj = (Teacher) obj;
		if (this.getName().equals(teacherObj.getName())) {
			if (this.getAge() == teacherObj.getAge()) {
				if (this.getEmail().equals(teacherObj.getEmail())) {
					return true;
				}
				return false;
			}
			return false;
		}
		return this.getAge() == teacherObj.getAge();
	}
	
	@Override
	public String toString() {
		return "Teacher [name=" + name + ", age=" + age + ", email=" + email + "]";
	}
}
