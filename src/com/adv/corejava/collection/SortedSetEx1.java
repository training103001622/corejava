package com.adv.corejava.collection;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetEx1 {

	public static void main(String[] args) {
		SortedSet<String> sortSet = new TreeSet<String>();
		sortSet.add("TigerZ");
		sortSet.add("Tiger");
		sortSet.add("TigerC");
		sortSet.add("Jimmi");
		sortSet.add("Anand");
		sortSet.add("Sunder");
		sortSet.add("Babu");
		sortSet.add(null);
		
		System.out.println(sortSet);
		
		System.out.println("Iterate using for iterator......");
		Iterator<String> iteratorObj = sortSet.iterator();
		while (iteratorObj.hasNext()) {
			String str = iteratorObj.next();
			System.out.println(str);
		}
	}
}
