package com.adv.corejava.collection;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedListEx1 {

	public static void main(String[] args) {
		LinkedList<String> strLinkedList = new LinkedList<>();
		strLinkedList.add("Jimmi");
		strLinkedList.add("Zebra");
		strLinkedList.add("Lion");
		strLinkedList.add("Fox");
		strLinkedList.add("Cow");
		strLinkedList.add("Zebra");
		System.out.println("strLinkedList="+strLinkedList);
		
		System.out.println("Iterate using for loop......");
		//iterate using for loop
		for(int i=0; i<strLinkedList.size(); i++) {
			System.out.println(strLinkedList.get(i));
		}
		
		System.out.println("Iterate using for iterator......");
		Iterator<String> iteratorObj = strLinkedList.iterator();
		while (iteratorObj.hasNext()) {
			String str = iteratorObj.next();
			System.out.println(str);
		}
		
		System.out.println("Iterate using for ListIterator......");
		ListIterator<String> listIterator = strLinkedList.listIterator();
		while (listIterator.hasNext()) {
			String currentStr = listIterator.next();
			System.out.println("Current Str="+currentStr);
		}
	}
}
