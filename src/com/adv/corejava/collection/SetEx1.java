package com.adv.corejava.collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetEx1 {

	public static void main(String[] args) {
		Set<String> set = new HashSet<>();
		set.add("TigerA");
		set.add("Tiger");
		set.add("Tiger");
		set.add("JimmiB");
		set.add("Tiger");
		set.add("Tiger");
		set.add("JimmiA");
		set.add(null);
		set.add(null);
		System.out.println("The value in set -> "+set);
		
		System.out.println("Iterate using for iterator......");
		Iterator<String> iteratorObj = set.iterator();
		while (iteratorObj.hasNext()) {
			String str = iteratorObj.next();
			System.out.println(str);
		}
	}
}
