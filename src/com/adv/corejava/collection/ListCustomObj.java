package com.adv.corejava.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListCustomObj {

	public static void main(String[] args) {
		List<Student> stdList = new ArrayList<>();

		Student s1 = new Student("A", 10, "a@g.com");
		Student s2 = new Student("X", 9, "x@g.com");
		Student s3 = new Student("Cow", 19, "19cow@g.com");
		Student s4 = new Student("Fox", 22, "fox@g.com");
		Student s5 = new Student("Elephant", 18, "18e@g.com");

		stdList.add(s1);
		stdList.add(s2);
		stdList.add(s3);
		stdList.add(s4);
		stdList.add(s5);

		String str1 = new String("Tiger");
		String str2 = new String("Tiger");
		
		if (str1 == str2) {
			System.out.println("Both Strings are Equal");
		} else {
			System.out.println("Both Strings are not Equal");
		}
		
		if (str1.equals(str2)) {
			System.out.println("Both Strings content are Equal");
		} else {
			System.out.println("Both Strings content are not Equal");
		}
		
		
		System.out.println("Before sorting. stdList=" + stdList);

		Collections.sort(stdList);
		
		//Collections.sort(stdList, new NameSorter());
		Collections.sort(stdList, new AgeSorter());

		System.out.println("After sorting. stdList=" + stdList);
	}
}

class Student  implements Comparable<Student>  {

	private String name;

	private int age;

	private String email;

	public Student(String name, int age, String email) {
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "[name=" + name + ", age=" + age + ", email=" + email + "]";
	}
	
	@Override
	public int compareTo(Student o) {
		return this.getName().compareTo(o.getName());
	}
}

class NameSorter implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		return o1.getName().compareTo(o2.getName());
	}
}

class AgeSorter implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		return o1.getAge() - o2.getAge();
	}
}
