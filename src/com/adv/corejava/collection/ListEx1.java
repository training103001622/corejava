package com.adv.corejava.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListEx1 {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
//		list.add("Tiger");
//		list.add("Tiger");
//		list.add("Tiger");
//		list.add("Tiger");
//		list.add("Tiger");
//		list.add("Tiger");
		list.add("A");
		list.add("X");
		list.add("M");
		list.add("C");
		list.add("B");
		System.out.println(list);
		
		System.out.println("Iterate using for loop......");
		//iterate using for loop
		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		System.out.println("Iterate using for iterator......");
		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext()) {
			String str = iterator.next();
			System.out.println(str);
		}
		
		System.out.println("Iterate using for ListIterator......");
		ListIterator<String> listIterator = list.listIterator();
		while (listIterator.hasNext()) {
			String currentStr = listIterator.next();
			System.out.println("Current Str="+currentStr);
		}
		
		//remove element from list
//		list.remove("C");
//		list.remove(2);
		
		System.out.println("Sort the list elements in ascending order...");
		Collections.sort(list);
		System.out.println("List after sort="+list);
	}
}
