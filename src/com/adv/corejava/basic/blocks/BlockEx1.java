package com.adv.corejava.basic.blocks;

public class BlockEx1 {

	private String name;
	
	private int age;

	static {
		System.out.println("Inside the static blocks");
	}
	
	{
		System.out.println("Inside the blocks-1");
		name = "Lion";
		age = 15;
	}
	
	{
		System.out.println("Inside the blocks-2");
	}
	
	public BlockEx1() {
		System.out.println("Inside the constructors-1");
		name = "Tiger";
		age = 10;
	}
	
	public BlockEx1(String name, int age) {
		System.out.println("Inside the constructors-2");
		name = "Tiger";
		age = 10;
	}
}
