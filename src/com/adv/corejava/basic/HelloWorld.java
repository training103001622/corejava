package com.adv.corejava.basic;

public class HelloWorld {

	//private String name = "Tiger";

	public static void m1() {
		System.out.println("Inside method m1()......");
	}

	public void m2() {
		System.out.println("Inside method m2......");
	}

	public static void main(String ar[]) {
		System.out.println("Hello World.....Modified...");
		m1();//if this static method is in same class, we can call it directly
		HelloWorld.m1();
		HelloWorld hw = new HelloWorld();
		hw.m2();
		hw.m2();
		hw.m2();
		hw.m2();
		System.out.println("----------------------------------");
		Test t1 = new Test();
		t1.m3();
		t1.m4();//calling static method using object
		Test.m4();//calling static method using class name
	}
}