package com.adv.corejava.basic.constructors;

import java.util.Scanner;

public class TestA {

	public static void main(String[] args) {
		A a = new A();
		System.out.println(a.fName);
		//Accessing static variable by object
		System.out.println(a.lName);
		//Accessing static variable by class name
		System.out.println(A.lName);
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter str1 ::");
		String str1 = input.nextLine();
		System.out.println("Enter str2 ::");
		String str2 = input.nextLine();
		System.out.println("Enter val1 ::");
		int val1 = input.nextInt();
		System.out.println("Enter val2 ::");
		int val2 = input.nextInt();
//		System.out.println("Enter res ::");
//		boolean res = input.nextBoolean();
		
		//System.out.println("str1="+str1+", str2="+str2);
		
		int result =a.add(val1, val2);
		System.out.println("result="+result);
		
		System.out.println(val1 +", "+ val2 +", "+ str1 +", "+ str2);
		
		String s1 = "Hi, Welcome to Advento\nYour Training will start soon \nDon't take any leave";
		System.out.println(s1);
	}
}
