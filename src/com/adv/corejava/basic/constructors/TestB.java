package com.adv.corejava.basic.constructors;

public class TestB {

	public static void main(String[] args) {
		B b1 = new B();
		System.out.println("1. Name = "+b1.name + ", Age="+b1.age);
		B b2 = new B(10);
		System.out.println("2. Name = "+b2.name + ", Age="+b2.age);
		B b3 = new B("Zebra");
		System.out.println("3. Name = "+b3.name + ", Age="+b3.age);
		B b4 = new B("Fox", 15);
		System.out.println("4. Name = "+b4.name + ", Age="+b4.age);
	}
}
