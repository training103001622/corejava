package com.adv.corejava.basic.constructors;

public class B {

	public String name;
	
//	public String email;
	
	public int age;
	
//	{
//		this.name = "jimmy";
//		this.age = 7;
//	}
	
	static {
	}
	
	public B() {
//		{
//			this.name = "jimmy";
//			this.age = 7;
//		}
		this.name = "Elephant";
		this.age = 20;
	}
	
	public B(String name) {
		this();
		this.name = name;
	}
	
	public B(int age) {
		this();
		this.age = age;
	}
	
	public B(String name, int age) {
		this();
		this.name = name;
		this.age = age;
	}
	
//	public B(int age, String name) {
//		this.name = name;
//		this.age = age;
//	}
//	
//	public B(String email, String name) {
//		this.name = name;
//		this.email = email;
//	}
//	
//	public B(String name, String email) {
//		this.name = name;
//		this.email = email;
//	}
	
	
}
