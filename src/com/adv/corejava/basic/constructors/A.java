package com.adv.corejava.basic.constructors;

public class A {

	public String fName = "Tiger";
	
	public static String lName = "Lion";
	
	public A() {
	}
	
	public int add() {
		int a = 5;
		int b = 10;
		int c = a + b;
		return c;
	}
	
	public int add(int a, int b) {
		int c = a + b;
		return c;
	}
}
