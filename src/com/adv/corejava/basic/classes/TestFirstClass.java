package com.adv.corejava.basic.classes;

public class TestFirstClass {

	public static void main(String[] args) {
		FirstClass firstClassObj1 = new FirstClass(15);
		
		FirstClass firstClassObj2 = new FirstClass();
//		firstClass.age = 10;
		System.out.println(firstClassObj2.getAge());
		firstClassObj2.setAge(9);
		System.out.println(firstClassObj2.getAge());
//		firstClass.age = 20;
		System.out.println(firstClassObj2.getAge());
	}
}
