package com.adv.corejava.basic.classes;

public class FirstClass {
	
	private int age;
	
	static {
		System.out.println("Inside the static blocks1");
	}
	
	static {
		System.out.println("Inside the static blocks2");
	}

	{
		System.out.println("Inside the blocks1");
		age = 10;
	}
	
	{
		System.out.println("Inside the blocks2");
		age = 10;
	}
	
	public FirstClass() {
		System.out.println("Inside the default constructor");
		this.age = 10;
	}
	
	public FirstClass(int age) {
		System.out.println("Inside the parameterized constructor");
		this.age = age;
	}
	
	public synchronized void setAge(int age) {
		if (age > 10) {
			this.age = age;
		}
	}
	
	public int getAge() {
		return age;
	}
}
