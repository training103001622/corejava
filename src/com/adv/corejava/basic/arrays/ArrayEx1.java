package com.adv.corejava.basic.arrays;

public class ArrayEx1 {

	public int[] getEvenArray(int start, int end) {
		int a[] = new int[(end-start)/2];
		int k = 0;
		for (int i = start; i < end; i++) {
			if (i%2 == 0) {
				a[k++] = i;
			}
		}
		return a;
	}
	
	public void printArr(int ar[]) {
		for (int i = 0; i < ar.length; i++) {
			System.out.print(ar[i]+", ");
		}
	}
	
	public static void main(String[] args) {
//		//array declaration and object creation but not assigned with values
//		int a[] = new int[10];
//		int[] b = new int[10];
//		//direct initialization
//		int c[] = {10, 15, 20};
		
		ArrayEx1 arrayEx1 = new ArrayEx1();
		int res[] = arrayEx1.getEvenArray(15, 25);
		arrayEx1.printArr(res);
	}
}
