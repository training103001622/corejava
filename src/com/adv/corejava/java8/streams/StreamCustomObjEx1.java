package com.adv.corejava.java8.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamCustomObjEx1 {

	public static void main(String[] args) {
		List<Student> stdList = new ArrayList<>();
		stdList.add(new Student("Arun", 17));
		stdList.add(new Student("Anand", 10));
		stdList.add(new Student("Anand", 4));
		stdList.add(new Student("Babu", 12));
		stdList.add(new Student("Anbu", 17));
		stdList.add(new Student("Kuppu", 15));
		stdList.add(new Student("Suresh", 8));
		stdList.add(new Student("Arul", 6));
		stdList.add(new Student("Kannan", 19));
		stdList.add(new Student("Anand", 15));
		stdList.add(new Student("Babu", 17));
		stdList.add(new Student("Kannan", 30));
		stdList.add(new Student("Arun", 7));
		
		long totalValues = stdList.parallelStream().filter(x -> x.getName().startsWith("A")).count();
		System.out.println(totalValues);
		
		List<Student> newStdList = stdList.parallelStream().filter(x -> x.getName().startsWith("A")).collect(Collectors.toList());
		System.out.println(newStdList);
		
		Set<Student> newStdSet = stdList.parallelStream().filter(x -> x.getName().startsWith("A")).collect(Collectors.toSet());
		System.out.println(newStdSet);
		
		List<Student> stdListWithoutDuplicates = stdList.parallelStream().filter(x -> x.getName().startsWith("A")).distinct().collect(Collectors.toList());
		System.out.println(stdListWithoutDuplicates);
		
		List<Student> stdListSorted = stdList.parallelStream().filter(x -> x.getName().startsWith("A")).sorted().collect(Collectors.toList());
		System.out.println(stdListSorted);
		
		List<Student> stdListAgeSort = stdList.parallelStream().filter(x -> x.getName().startsWith("A")).sorted((s1, s2) -> s2.getAge() - s1.getAge()).collect(Collectors.toList());
		System.out.println(stdListAgeSort);
		
		//Function<T, R>
		
		
		
		Map<String, Long> stdListGroupbyName = stdList.parallelStream().collect(Collectors.groupingBy(Student :: getName, Collectors.counting()));
		System.out.println(stdListGroupbyName);
	}
}

class Student implements Comparable<Student> {
	
	private String name;
	
	private int age;

	public Student(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int compareTo(Student o) {
		return this.getName().compareTo(o.getName());
	}
	
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}
}
