package com.adv.corejava.java8.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamEx1 {

	public static void main(String[] args) {
		List<String> strList = new ArrayList<>();
		strList.add("A1");
		strList.add("A2");
		strList.add("B1");
		strList.add("B2");
		strList.add("C1");
		strList.add("A3");
		strList.add("C2");
		strList.add("A2");
		
		long totalValues = strList.parallelStream().filter(x -> x.startsWith("A")).count();
		System.out.println(totalValues);
		
		List<String> newStrList = strList.parallelStream().filter(x -> x.startsWith("A")).collect(Collectors.toList());
		System.out.println(newStrList);
		
		Set<String> newStrSet = strList.parallelStream().filter(x -> x.startsWith("A")).collect(Collectors.toSet());
		System.out.println(newStrSet);
		
		List<String> strListWithoutDuplicates = strList.parallelStream().filter(x -> x.startsWith("A")).distinct().collect(Collectors.toList());
		System.out.println(strListWithoutDuplicates);
	}
}
