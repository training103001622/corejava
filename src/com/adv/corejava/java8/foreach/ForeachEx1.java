package com.adv.corejava.java8.foreach;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ForeachEx1 {

	public static void main(String[] args) {
		List<Student> stdList = new ArrayList<>();
		stdList.add(new Student("Arun", 17));
		stdList.add(new Student("Anand", 10));
		stdList.add(new Student("Anand", 4));
		stdList.add(new Student("Babu", 12));
		stdList.add(new Student("Anbu", 17));
		stdList.add(new Student("Kuppu", 15));
		stdList.add(new Student("Suresh", 8));
		stdList.add(new Student("Arul", 6));
		stdList.add(new Student("Kannan", 19));
		stdList.add(new Student("Anand", 15));
		stdList.add(new Student("Babu", 17));
		stdList.add(new Student("Kannan", 30));
		stdList.add(new Student("Arun", 7));
		
		//iterator
		Iterator<Student> stdIterator = stdList.iterator();
		while (stdIterator.hasNext()) {
			Student student = (Student) stdIterator.next();
			System.out.println("Name="+student.getName()+", Age="+student.getAge());
		}
		
		//advanced forloop
		for (Student std : stdList) {
			System.out.println("Name="+std.getName()+", Age="+std.getAge());
		}
		
		//foreach
		stdList.forEach(d-> {
			System.out.println("Name="+d.getName()+", Age="+d.getAge());
		});
	}
}

class Student implements Comparable<Student> {
	
	private String name;
	
	private int age;

	public Student(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int compareTo(Student o) {
		return this.getName().compareTo(o.getName());
	}
	
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}
}
