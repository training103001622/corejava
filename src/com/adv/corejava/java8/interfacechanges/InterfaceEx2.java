package com.adv.corejava.java8.interfacechanges;

//public class InterfaceEx2 {
//
//	public void m1(String var) {
//		String name = "Tiger";
//	}
//}

public abstract class InterfaceEx2 {
	
	//public static final String AGE_VALUE = "10";
	
	public abstract void m1();
	
	public abstract void m2();
}

abstract class ChildInterfaceImpl1 extends InterfaceEx2 {
	
	@Override
	public void m1() {
		System.out.println("Inside ChildInterfaceImpl - m1");
	}
}

class ChildInterfaceImpl2 extends ChildInterfaceImpl1 {
	
	@Override
	public void m2() {
		System.out.println("Inside ChildInterfaceImpl - m2");
	}
}
