package com.adv.corejava.java8.interfacechanges;

class InterfaceEx1 {

	public static void main(String[] args) {
		System.out.println("Inside main....");
		System.out.println("Name="+Interface1.NAME);
		Interface1.m3();
		Interface1 interface1 = new Child();
		interface1.m5();
	}
}

interface Interface1 {
	
	public static final String NAME = "Tiger";
	
	public abstract void m1();
	
	public void m2();
	
	public default void m5() {
		System.out.println("Inside m5 - Interface1");
	}
	
	public static void m3() {
		System.out.println("Inside m3......");
	}
	
	public default void m4() {
		System.out.println("Inside m4......");
	}
}

class Child implements Interface1 {
	
	@Override
	public void m1() {
	}
	
	@Override
	public void m2() {
		
	}
	
	@Override
	public void m5() {
		System.out.println("Inside m5 - Child");
	}
}