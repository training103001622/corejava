package com.adv.corejava.java8.lambda;

public class LambdaEx1 {

	public static void main(String[] args) {
		FI fi = new FIImpl();
		System.out.println(fi.add(100, 50));

		//anonymous impl
		FI fi1 = new FI() {
			
			@Override
			public int add(int a, int b) {
				return a + b;
			}
		};
		//lambda
		FI fi3 = (a, b) -> a+b;
		
		System.out.println("Lamda Impl1 -> "+fi3.add(5, 45));
		
		//anonymous impl
		FI fi2 = new FI() {
			
			@Override
			public int add(int a, int b) {
				int c = a + b;
				return c;
			}
		};
		//Lambda
		FI fi4 = (a, b) -> {
			int c = a + b;
			return c;
		};
		
		System.out.println("Lamda Impl2 -> "+fi4.add(15, 40));
	}
}

@FunctionalInterface
interface FI {
	
	public int add(int a, int b);
	
	//public int sub(int a, int b);
}

class FIImpl implements FI {
	
	@Override
	public int add(int a, int b) {
		return a + b;
	}
}