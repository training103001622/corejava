package com.adv.corejava.java8.methodreference;

public class MethodReferenceEx1 {

	public static void main(String[] args) {
		Employee emp = new Employee("Tiger", 10);
		System.out.println("Name="+emp.getName()+", Age="+emp.getAge());
		
		One one = emp::getName;
		
		System.out.println("Name="+one.execute()+", Age="+emp.getAge());
		
		One one1 = new OneImpl();
		System.out.println(one1.execute());
		
		One one2 = emp::toString;
		System.out.println(one2.execute());
		
		One one3 = Employee::printEmp;
		one3.execute();
		
		Two two = Employee :: new;
		Employee emp1 = two.execute("Lion", 15);
		System.out.println("Name="+emp1.getName()+", Age="+emp1.getAge());
	}
}

interface One {
	
	public String execute();
	
	public static void print() {
		System.out.println("Inside One - print.....");
	}
}

interface Two {
	
	public Employee execute(String val1, int val2);
}

class OneImpl implements One {
	
	@Override
	public String execute() {
		return "Inside OneImpl - execute";
	}
}


class Employee {
	
	private String name;
	
	private int age;
		
	public Employee(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public static String printEmp() {
		return "Inside Emp - printEmp.......";
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + "]";
	}
}