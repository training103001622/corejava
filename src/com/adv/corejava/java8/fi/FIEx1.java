package com.adv.corejava.java8.fi;

public class FIEx1 {

	public static void main(String[] args) {
		FI fi = new FIImpl();
		System.out.println(fi.add(100, 50));
		FI fi1 = new FI() {
			
			@Override
			public int add(int a, int b) {
				return a + b;
			}
		};
		fi1.add(10, 2);
	}
}

@FunctionalInterface
interface FI {
	
	public int add(int a, int b);
	
	//public int sub(int a, int b);
}

class FIImpl implements FI {
	
	@Override
	public int add(int a, int b) {
		return a + b;
	}
}