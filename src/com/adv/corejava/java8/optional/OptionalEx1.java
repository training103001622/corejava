package com.adv.corejava.java8.optional;

import java.util.Optional;

public class OptionalEx1 {

	public static void main(String[] args) {
		String str = null;
//		str.length();
		
//		Optional<String> optStr = Optional.of(str);
//		System.out.println(optStr);
		
		Optional<String> optStr1 = Optional.ofNullable(str);
		System.out.println(optStr1);
		
		Optional<String> optStr2 = Optional.empty();
		System.out.println(optStr2);
		
		optStr1.hashCode();
		if (optStr1.equals("Zebra")) {
			System.out.println("The Value is Zebra..........");
		} else {
			System.out.println("The Value is not Zebra..........");
		}
		
		
		if (optStr1.isPresent()) {
			String value = optStr1.get();
			System.out.println("Value="+value);
		}
		
		if (!optStr1.isEmpty()) {
			String value = optStr1.get();
			System.out.println("Value="+value);
		}
		
		optStr1.ifPresent(e -> System.out.println(e));
		
		optStr1.ifPresentOrElse(e -> System.out.println(e), () -> System.out.println("Value not Present"));
		
	}
}
