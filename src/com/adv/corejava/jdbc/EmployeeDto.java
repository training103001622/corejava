package com.adv.corejava.jdbc;

public class EmployeeDto {

	private int id;
	
	private String fName;
	
	private int age;
	
	private String email;

	public EmployeeDto(int id, String fName, int age, String email) {
		super();
		this.id = id;
		this.fName = fName;
		this.age = age;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "EmployeeDto [id=" + id + ", fName=" + fName + ", age=" + age + ", email=" + email + "]";
	}
}
