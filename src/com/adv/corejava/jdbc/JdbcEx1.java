package com.adv.corejava.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcEx1 {

	public void readOperation() {
		//5 steps
		Connection conn = null;
		try {
			conn = getConnection();
			//3. create statement
			Statement stmt = conn.createStatement();
			//4. execute query
			ResultSet rs = stmt.executeQuery("select id, first_name, age, email from employee");
			//5. iterate the result-set
			while(rs.next()) {
				int id = rs.getInt(1);
				String firstName = rs.getString(2);
				int age = rs.getInt(3);
				String email = rs.getString(4);
				System.out.println("id="+id+", firstName="+firstName+", age="+age+", email="+email);
			}
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public EmployeeDto[] readAndReturnEmpDto() {
		EmployeeDto[] employeeDtos = new EmployeeDto[10];
		//5 steps
		try (Connection conn = getConnection();){
			int i = 0;
			//3. create statement
			Statement stmt = conn.createStatement();
			//4. execute query
			ResultSet rs = stmt.executeQuery("select id, first_name, age, email from employee");
			//5. iterate the result-set
			while(rs.next() && i < 10) {
				int id = rs.getInt(1);
				String firstName = rs.getString(2);
				int age = rs.getInt(3);
				String email = rs.getString(4);
				EmployeeDto employeeDto = new EmployeeDto(id, firstName, age, email);
				employeeDtos[i++] = employeeDto;
			}
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		return employeeDtos;
	}
	
	public void createOperationWithStatement() {
		//4 steps
		Connection conn = null;
		try {
			conn = getConnection();
			//3. create statement
			Statement stmt = conn.createStatement();
			//4. execute query
			stmt.execute("insert into employee (id, first_name, age, email) values (5, 't5', 8, 't5@g.com')");
			System.out.println("Insert Operation successful.....");
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void createOperationWithStatement(int id, String fName, int age, String email) {
		//4 steps
		Connection conn = null;
		try {
			conn = getConnection();
			//3. create statement
			Statement stmt = conn.createStatement();
			//4. execute query
			stmt.execute("insert into employee (id, first_name, age, email) values ("+id+", "+fName+", "+age+", "+email+")");
			System.out.println("Insert Operation successful.....");
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void createOperationWithPreparedStatement(int id, String fName, int age, String email) {
		//4 steps
		try (Connection conn = getConnection();) {
			//3. create prepared statement
			PreparedStatement pstmt = conn.prepareStatement("insert into employee (id, first_name, age, email) values (?, ?, ?, ?)");
			pstmt.setInt(1, id);
			pstmt.setString(2, fName);
			pstmt.setInt(3, age);
			pstmt.setString(4, email);
			//4. execute query
			pstmt.execute();
			System.out.println("Insert Operation successful.....");
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}
	
	public void updateOperation(int id, String fName, String email) {
		//4 steps
		try (Connection conn = getConnection();) {
			//3. create prepared statement
			PreparedStatement pstmt = conn.prepareStatement("update employee set first_name=?, email=? where id=?");
			pstmt.setInt(3, id);
			pstmt.setString(1, fName);
			pstmt.setString(2, email);
			//4. execute query
			pstmt.execute();
			System.out.println("Update Operation successful.....");
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}
	
	public void deleteOperation(int id) {
		//4 steps
		try (Connection conn = getConnection();) {
			//3. create prepared statement
			PreparedStatement pstmt = conn.prepareStatement("delete from employee where id=?");
			pstmt.setInt(1, id);
			//4. execute query
			pstmt.execute();
			System.out.println("Delete Operation successful.....");
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection conn;
		//1. install driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		//2. establish connection
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/training10", "root", "root");
		return conn;
	}
	
	public void printArr(EmployeeDto[] employeeDtos) {
		for (int i = 0; i<employeeDtos.length; i++) {
			System.out.println(employeeDtos[i]);
		}
	}
}
