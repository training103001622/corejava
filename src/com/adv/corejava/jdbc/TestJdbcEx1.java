package com.adv.corejava.jdbc;

public class TestJdbcEx1 {

	public static void main(String[] args) {
		JdbcEx1 jdbcEx1 = new JdbcEx1();
		jdbcEx1.readOperation();
		System.out.println("-------------------------------");
		EmployeeDto[] employeeDtos = jdbcEx1.readAndReturnEmpDto();
		jdbcEx1.printArr(employeeDtos);
		System.out.println("-------------------------------");
		try {
			jdbcEx1.createOperationWithStatement();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			jdbcEx1.createOperationWithPreparedStatement(9, "t9", 19, "t9@g.com");
		} catch (Exception ex) {
			ex.printStackTrace();
		}	
		jdbcEx1.updateOperation(9, "t9_U", "t9_U@g.com");
		jdbcEx1.deleteOperation(5);
	}
}
