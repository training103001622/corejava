package com.adv.corejava.pack_test;

import com.adv.corejava.pack3.A;

public class TestA {

	public static void main(String[] args) {
		A a = new A();
		a.m1();
		
		com.adv.corejava.pack1.A a1 = new com.adv.corejava.pack1.A();
		a1.m1();
	}
}
